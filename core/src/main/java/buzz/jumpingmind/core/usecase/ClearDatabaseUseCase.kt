package buzz.jumpingmind.core.usecase

import buzz.jumpingmind.core.domain.database.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class ClearDatabaseUseCase @Inject constructor(
    private val appDatabase: AppDatabase
){
    suspend operator fun invoke(){
        appDatabase.clearAllTables()
    }
}