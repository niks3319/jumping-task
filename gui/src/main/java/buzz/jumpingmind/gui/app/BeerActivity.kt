package buzz.jumpingmind.gui.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import buzz.jumpingmind.base.activity.BaseActivity
import buzz.jumpingmind.base.failure
import buzz.jumpingmind.base.observe
import buzz.jumpingmind.core.domain.model.Beer
import buzz.jumpingmind.gui.R
import buzz.jumpingmind.gui.databinding.ActivityBeerBinding
import buzz.jumpingmind.gui.handleFailure
import buzz.jumpingmind.gui.showErrorToast
import com.google.android.material.appbar.AppBarLayout
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@AndroidEntryPoint
class BeerActivity : BaseActivity() {

    override val layoutResId = R.layout.activity_beer
    override val bindingInflater: (LayoutInflater) -> ViewBinding = ActivityBeerBinding::inflate
    override val binding: ActivityBeerBinding get() = super.binding as ActivityBeerBinding

    private val viewModel: BeerViewModel by viewModels()
    private var isToolbarExpended: Boolean = false
    private var scrollRange: Int = -1

    @Inject
    internal lateinit var beerAdapter: BeerCardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.statusBarColor = this.let { ContextCompat.getColor(it, R.color.black) }

        viewModel.apply {
            failure(failure, ::handleError)
            observe(beers, ::onBeerLoaded)
            loadBeer()
        }

        binding.apply {
            rvBeer.apply {
                adapter = beerAdapter
                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                        if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                            Timber.d("Need to load more beer")
                            binding.pbLoading.visibility = VISIBLE
                            viewModel.loadBeer()
                        }
                    }
                })
            }

        }
    }

    private fun onBeerLoaded(beers: List<Beer>) {
        beerAdapter.addToBeerList(beers)
        binding.pbLoading.visibility = GONE
    }

    private fun handleError(exception: Exception?){
        viewModel.loadLocalBeer()
    }

}