package buzz.jumpingmind.core.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Beer(
    @PrimaryKey
    val id: Int,

    @ColumnInfo
    val name: String,

    @ColumnInfo
    val tagLine: String,

    @ColumnInfo
    val firstBrewed: String,

    @ColumnInfo
    val description: String,

    @ColumnInfo
    val image: String,

    @ColumnInfo
    val attenuationLevel: Float
)