package buzz.jumpingmind.core.domain.webservice

import buzz.jumpingmind.base.exception.Failure.DataFailure

class WebServiceFailure {
    class NoNetworkFailure(
        msg: String = "Network not available!"
    ) : DataFailure(msg)

    class NetworkTimeOutFailure(
        msg: String = "Network timeout!"
    ) : DataFailure(msg)

    class NetworkDataFailure(
        msg: String = "Error parsing data!"
    ) : DataFailure(msg)

    class UnknownNetworkFailure(
        msg: String = "Unknown network error!"
    ) : DataFailure(msg)
}