package buzz.jumpingmind.core.usecase

import buzz.jumpingmind.core.domain.database.AppDatabase
import buzz.jumpingmind.core.domain.database.DatabaseModule
import buzz.jumpingmind.core.domain.model.Beer
import buzz.jumpingmind.core.domain.webservice.BeerWebService
import javax.inject.Inject

class GetBeerUseCase @Inject constructor(
    private val beerWebService: BeerWebService
) {

    suspend operator fun invoke(pageNo: Int) : List<Beer>{
        return beerWebService.getBears(pageNo)

    }
}