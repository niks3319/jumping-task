package buzz.jumpingmind.network.responses

import buzz.jumpingmind.core.domain.model.Beer
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

internal fun GetBeerResponse.toBeer() = Beer(
    id = id,
    name = name,
    tagLine = tagline,
    firstBrewed = firstBrewed,
    description = description,
    image = imageUrl,
    attenuationLevel = attenuationLevel
)

@JsonClass(generateAdapter = true)
internal class GetBeerResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "tagline")
    val tagline: String,
    @Json(name = "first_brewed")
    val firstBrewed: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "attenuation_level")
    val attenuationLevel: Float,
    @Json(name = "image_url")
    val imageUrl: String,
)
