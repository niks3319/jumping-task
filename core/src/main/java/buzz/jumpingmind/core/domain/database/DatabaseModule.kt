package buzz.jumpingmind.core.domain.database

import buzz.jumpingmind.core.domain.database.BeerDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideBeerDao(appDatabase: AppDatabase): BeerDao {
        return appDatabase.beerDao()
    }
}