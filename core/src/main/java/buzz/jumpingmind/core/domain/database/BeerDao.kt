package buzz.jumpingmind.core.domain.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import buzz.jumpingmind.core.domain.model.Beer

@Dao
interface BeerDao {

    @Query("SELECT * FROM beer")
    fun getAllBeers(): List<Beer>

    @Insert
    fun insertBeers(beer: Beer)
}