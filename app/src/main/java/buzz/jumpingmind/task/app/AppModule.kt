package buzz.jumpingmind.task.app

import android.content.Context
import androidx.room.Room
import buzz.jumpingmind.core.domain.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideYourDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        "beer_db"
    ).build()

    @Singleton
    @Provides
    fun provideYourDao(db: AppDatabase) = db.beerDao()
}