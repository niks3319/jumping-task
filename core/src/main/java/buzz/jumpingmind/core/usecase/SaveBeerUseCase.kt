package buzz.jumpingmind.core.usecase

import buzz.jumpingmind.core.domain.database.AppDatabase
import buzz.jumpingmind.core.domain.model.Beer
import javax.inject.Inject

class SaveBeerUseCase @Inject constructor(
    private val appDatabase: AppDatabase
) {

    suspend operator fun invoke(list: List<Beer>){
        list.forEach {
            appDatabase.beerDao().insertBeers(it)
        }
    }
}