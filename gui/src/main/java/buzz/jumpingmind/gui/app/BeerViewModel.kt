package buzz.jumpingmind.gui.app

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import buzz.jumpingmind.base.viewmodel.BaseViewModel
import buzz.jumpingmind.core.domain.model.Beer
import buzz.jumpingmind.core.usecase.ClearDatabaseUseCase
import buzz.jumpingmind.core.usecase.GetBeerUseCase
import buzz.jumpingmind.core.usecase.GetLocalBeerUseCase
import buzz.jumpingmind.core.usecase.SaveBeerUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BeerViewModel @Inject constructor(
    private val getBeerUseCase: GetBeerUseCase,
    private val saveBeerUseCase: SaveBeerUseCase,
    private val clearDatabaseUseCase: ClearDatabaseUseCase,
    private val getLocalBeerUseCase: GetLocalBeerUseCase

): BaseViewModel(){

    private var beerPageNo: Int = 0

    private val _beers:MutableLiveData<List<Beer>> by lazy { MutableLiveData<List<Beer>>() }
    internal val beers : LiveData<List<Beer>> = _beers

    internal fun loadBeer(){
        launchUseCases {
            val beerList = getBeerUseCase(beerPageNo)
            if(beerPageNo==0){
                clearDatabaseUseCase.invoke()
            }
            _beers.postValue(beerList)
            saveBeerUseCase.invoke(beerList)
            beerPageNo+=1
        }
    }

    internal fun loadLocalBeer(){
        launchUseCases {
            _beers.postValue(getLocalBeerUseCase())
        }
    }
}