package buzz.jumpingmind.core.domain.webservice

import buzz.jumpingmind.core.domain.model.Beer

interface BeerWebService {

    suspend fun getBears(pageNo: Int): List<Beer>
}