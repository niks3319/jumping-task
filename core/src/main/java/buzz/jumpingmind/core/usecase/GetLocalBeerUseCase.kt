package buzz.jumpingmind.core.usecase

import buzz.jumpingmind.core.domain.database.AppDatabase
import buzz.jumpingmind.core.domain.model.Beer
import javax.inject.Inject

class GetLocalBeerUseCase @Inject constructor(
    private val appDatabase: AppDatabase
){

    suspend operator fun invoke() : List<Beer>{
        return appDatabase.beerDao().getAllBeers()
    }
}