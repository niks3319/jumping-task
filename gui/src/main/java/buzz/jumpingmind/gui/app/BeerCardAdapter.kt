package buzz.jumpingmind.gui.app

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import buzz.jumpingmind.base.layoutInflater
import buzz.jumpingmind.core.domain.model.Beer
import buzz.jumpingmind.gui.R
import buzz.jumpingmind.gui.databinding.TileBeerCardViewBinding
import buzz.jumpingmind.gui.loadUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject
import kotlin.properties.Delegates

@ActivityScoped
internal class BeerCardAdapter @Inject constructor() :
    RecyclerView.Adapter<BeerCardAdapter.BeerCardAdapterVM>() {

    internal var beers: List<Beer> by Delegates.observable(arrayListOf()) { _, _, _ -> notifyDataSetChanged() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerCardAdapterVM =
        BeerCardAdapterVM(
            TileBeerCardViewBinding.inflate(parent.layoutInflater, parent, false)
        )

    override fun onBindViewHolder(holder: BeerCardAdapterVM, position: Int) {
        holder.bind(beers[position])
    }

    override fun getItemCount(): Int = beers.size

    internal fun addToBeerList(beerList: List<Beer>){
        beers = if(beers.isEmpty()){
            beerList
        } else{
            val result: MutableList<Beer> = mutableListOf()
            result.addAll(beers)
            result.addAll(beerList)
            result
        }
        notifyDataSetChanged()
    }

    inner class BeerCardAdapterVM(private val binding: TileBeerCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(beer: Beer) {
            binding.apply {
                ivBeerImg.loadUrl(
                    beer.image,
                    null,
                    ImageView.ScaleType.CENTER_INSIDE
                )

                tvBeerName.text = beer.name
                tvBeerTagline.text = beer.tagLine
                tvFirstBrew.text = beer.firstBrewed.removeRange(0,3)
                tvBeerDesc.text = beer.description
                val beerValue = beer.attenuationLevel.toInt()
                when {
                    beerValue > 80 ->{
                        pbValue.setIndicatorColor(ContextCompat.getColor(pbValue.context, android.R.color.holo_green_dark))
                        tvValueIndicator.text = tvValueIndicator.context.getString(R.string.value_rare)
                    }

                    beerValue > 50 ->{
                        pbValue.setIndicatorColor(ContextCompat.getColor(pbValue.context, android.R.color.holo_orange_light))
                        tvValueIndicator.text = tvValueIndicator.context.getString(R.string.value_moderate)
                    }

                    else ->{
                        pbValue.setIndicatorColor(ContextCompat.getColor(pbValue.context, android.R.color.holo_red_light))
                        tvValueIndicator.text = tvValueIndicator.context.getString(R.string.value_common)
                    }
                }

                pbValue.progress = beerValue
            }
        }

    }

}