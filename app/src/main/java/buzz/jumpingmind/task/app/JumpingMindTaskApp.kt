package buzz.jumpingmind.task.app

import android.app.Application
import buzz.jumpingmind.base.displayName
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class JumpingMindTaskApp: Application() {

    init{
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun onCreate() {
        Timber.v("onCreate $displayName")
        super.onCreate()
    }
}