package buzz.jumpingmind.network.services

import buzz.jumpingmind.core.domain.model.Beer
import buzz.jumpingmind.core.domain.webservice.BeerWebService
import buzz.jumpingmind.network.responses.toBeer
import javax.inject.Inject

internal class AppBeerWebService @Inject constructor(
    private val retrofitBeerWebService: RetrofitBeerWebService
) : BeerWebService {

    override suspend fun getBears(pageNo: Int): List<Beer> = networkCall(
        { retrofitBeerWebService.getBeer(pageNo) },
        { response -> response.map { it.toBeer() } }
    )
}