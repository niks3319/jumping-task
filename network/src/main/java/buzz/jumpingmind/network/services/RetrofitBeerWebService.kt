package buzz.jumpingmind.network.services

import buzz.jumpingmind.network.responses.GetBeerResponse
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RetrofitBeerWebService {

    @GET("beers")
    suspend fun getBeer(
        @Query("pages", encoded = true) pageNo: Int
    ): List<GetBeerResponse>
}